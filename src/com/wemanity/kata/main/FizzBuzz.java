package com.wemanity.kata.main;

public class FizzBuzz {

    public FizzBuzz(){

    }

    public String calculateFizzBuzz(int number ){

        throwExceptionIfNumberIsOutOfRange(number);

        boolean isFizz = isFizz(number);
        boolean isBuzz = isBuzz(number);

            if (isFizz && isBuzz) {
                return "FizzBuzzBuzz";
            } else if (isFizz) {
                return "Fizz";
            } else if (isBuzz) {
                return "Buzz";
            } else {
                return String.valueOf(number);
            }

    }

    private boolean isFizz( int number){
       return number % 3 == 0 || String.valueOf(number).contains("3");
    }

    private boolean isBuzz( int number){
        return number % 5 == 0 || String.valueOf(number).contains("5");
    }

    private void throwExceptionIfNumberIsOutOfRange(int number) {
        if (number < 1) {
            throw new IllegalArgumentException("Number must be greater than 0");
        } else if (number > 100) {
            throw new IllegalArgumentException("Number must be less than 101");
        }
    }
}
