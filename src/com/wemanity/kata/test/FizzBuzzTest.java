package com.wemanity.kata.test;

import com.wemanity.kata.main.FizzBuzz;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FizzBuzzTest {
    @Test
    public void testFizz() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.calculateFizzBuzz(9);
        assertEquals("Fizz", result);
    }

    @Test
    public void testBuzz() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.calculateFizzBuzz(25);
        assertEquals("Buzz", result);
    }

    @Test
    public void testNumber() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.calculateFizzBuzz(7);
        assertEquals("7", result);
    }

    @Test
    public void testFizzContains() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.calculateFizzBuzz(13);
        assertEquals("Fizz", result);
    }

    @Test
    public void testBuzzContains() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.calculateFizzBuzz(52);
        assertEquals("Buzz", result);
    }

    @Test
    public void testFizzBuzzContains() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.calculateFizzBuzz(35);
        assertEquals("FizzBuzzBuzz", result);
    }
}



